<?php

/**
 * Callback function to supply a list of content types.
 */
function term_display_node_terms_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Taxonomy terms'),
    'icon' => 'icon_term.png',
    'icon path' => drupal_get_path('module', 'ctools') . '/plugins/content_types/term_context',
    'description' => t('The assigned terms of the referenced node.'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'category' => t('Node'),
    'defaults' => array(
      'vocabulary' => NULL,
      'style' => TERM_DISPLAY_LIST,
    ),
  );
}

/**
 * Render the custom content type.
 */
function term_display_node_terms_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return;
  }

  // Get a shortcut to the node.
  $node = $context->data;
  $vocabulary = taxonomy_vocabulary_load($conf['vocabulary']);
  if (!isset($node->taxonomy) || !$vocabulary) {
    return;
  }

  $terms = array();
  foreach ($node->taxonomy as $tid => $term) {
    if ($term->vid == $conf['vocabulary']) {
      $terms[$tid] = $term;
    }
  }
  if (empty($terms)) {
    return;
  }

  // Build the content type block.
  $block = new stdClass();
  $block->module = 'term_display';
  $block->delta = $conf['vocabulary'];
  $block->title = check_plain($vocabulary->name);
  $block->content = theme('term_display_' . $conf['style'], $vocabulary, $terms);

  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function term_display_node_terms_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array();
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $options[$vid] = check_plain($vocabulary->name);
  }
  $form['vocabulary'] = array(
    '#type' => 'radios',
    '#title' => t('Vocabulary to display'),
    '#options' => $options,
    '#default_value' => $conf['vocabulary'],
  );

  $options = array(
    TERM_DISPLAY_LIST => t('List'),
    TERM_DISPLAY_CUSTOM => t('Custom (defaults to comma separated)'),
  );
  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Display style'),
    '#options' => $options,
    '#default_value' => $conf['style'],
  );
}

/**
 * Submit handler for the custom type settings form.
 */
function term_display_node_terms_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a type.
 */
function term_display_node_terms_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" taxonomy terms', array('@s' => $context->identifier));
}
